# Omniepay-Saferpay
omnipay-saferpay driver for the Omnipay PHP payment processing library.

[Omnipay](https://github.com/omnipay/omnipay) is a framework agnostic, multi-gateway payment
processing library for PHP. This package implements Alipay support for Omnipay.


# Installation

You can install the library by adding the sinnept code to you `composer.json` file

```json
    // [...]
    "repositories": [
        {
        "type": "vcs",
        "url": "https://github.com/FabianHippmann/laravel-snippet-manager.git" // PLEASE CHANGE THE PATH
        }
    ],
    // [...]
```
Save your file and run `composer install`.

# Usage

There are two main operation which are:
- `Purchase` -> To make payment transection with Saferpay gateway.
- `Capture` -> To make sure the transaction has been captured and purchase has been occured

## Purchase request implementation

You can use the folloing implementation to do purchase request.

```php
    use Omnipay\Omnipay;

    $gateway = Omnipay::create('Saferpay');
    $gateway->initialize([
        'userId' => 'USER ID',
        'password' => 'password',
        'transactionId' => 'Order ID',
        'customerId' => 'Customer ID',
        'terminalId' => 'Terminal Id',
        'description' => "Product description",
        'returnUrl' => 'Success URL',
        'cancelUrl' => 'Failure URL'
    ]);

    // In case of local development
    $gateway->setTestMode(true);

    $test = $gateway->purchase([
        'amount' => '100',
        'currency' => 'EUR',
    ]);
    $response = $test->send();
    if ($response->isSuccessful()) {
        var_dump($response->getRedirectUrl());
    } else {
        var_dump($response->getMessage());
    }
```

On success response you will get `token` which is important for verifying your transaction later (next process).

## Capture request implementation

Here you have to make sure that the end-server captured your payment transaction.

```php
    use Omnipay\Omnipay;

    $gateway = Omnipay::create('Saferpay');
    $gateway->initialize([
        'userId' => 'USER ID',
        'password' => 'password',
        'customerId' => 'Customer ID',
        'token' => 'Token', // This token is the returned token from end-server (Saferpay) gateway after making purchase method with response of 200.
    ]);
    
    // In case of local development
    $gateway->setTestMode(true);

    $response = $gateway2->capture()->send();

    if ($response->isSuccessful()) {
        var_dump($response->getData());
        // Your success code here
    } else {
        var_dump($response->getMessage());
    }
```

# Support (PLEASE CHANGE LINKS)

If you are having general issues with Omnipay, we suggest posting on
[Stack Overflow](http://stackoverflow.com/). Be sure to add the
[omnipay tag](http://stackoverflow.com/questions/tagged/omnipay) so it can be easily found.

If you want to keep up to date with release anouncements, discuss ideas for the project,
or ask more detailed questions, there is also aan [email](change@email.com) which
you can send you issue to.

If you believe you have found a bug, please report it using the [GitHub issue tracker](https://github.com/moonshiner/omnipay-saferpay/issues),
or better yet, fork the library and submit a pull request.

# License

PLEASE REFILL ACCORDINGLY