<?php

namespace Omnipay\Saferpay;

use Omnipay\Common\AbstractGateway;

class Gateway extends AbstractGateway
{
    public function getName()
    {
        return 'Saferpay';
    }

    // By using this method you can set default parameters if not exists during configuration.
    // public function getDefaultParameters()

    public function getUserId()
    {
        return $this->getParameter('userId');
    }

    public function setUserId($value)
    {
        return $this->setParameter('userId', $value);
    }

    public function setPassword($value)
    {
        return $this->setParameter('password', $value);
    }

    public function getEntityId()
    {
        return $this->getParameter('entityId');
    }

    public function setTerminalId($tid)
    {
        return $this->setParameter('terminalId', $tid);
    }

    public function setCustomerId($cid)
    {
        return $this->setParameter('customerId', $cid);
    }

    public function setDescription($value)
    {
        return $this->setParameter('description', $value);
    }

    public function setReturnUrl($rurl)
    {
        return $this->setParameter('returnUrl', $rurl);
    }

    public function setCancelUrl($curl)
    {
        return $this->setParameter('cancelUrl', $curl);
    }

    public function setToken($token)
    {
        return $this->setParameter('token', $token);
    }

    public function setTransactionId($tid)
    {
        return $this->setParameter('transactionId', $tid);
    }

    
    public function initialize(array $options = array())
    {
        return parent::initialize($options);
    }

    public function capture(array $options = array())
    {
        return $this->createRequest('\Omnipay\Saferpay\Message\CaptureRequest', $options);
    }

    public function purchase(array $options = array())
    {
        return $this->createRequest('\Omnipay\Saferpay\Message\PurchaseRequest', $options);
    }
}
