<?php

namespace Omnipay\Saferpay\Message;

use SebastianBergmann\CodeCoverage\Driver\Xdebug;


abstract class AbstractRequest extends \Omnipay\Common\Message\AbstractRequest
{

    protected $testEndpoint = 'https://test.saferpay.com';

    protected $liveEndpoint = 'https://www.saferpay.com';

    public function getUserId()
    {
        return $this->getParameter('userId');
    }

    public function setUserId($value)
    {
        return $this->setParameter('userId', $value);
    }

    public function getPassword()
    {
        return $this->getParameter('password');
    }

    public function setPassword($value)
    {
        return $this->setParameter('password', $value);
    }

    public function setTerminalId($tid)
    {
        return $this->setParameter('terminalId', $tid);
    }

    public function setCustomerId($cid)
    {
        return $this->setParameter('customerId', $cid);
    }

    public function setToken($token)
    {
        return $this->setParameter('token', $token);
    }

    public function getData()
    {
        return $this->parameters->all();
    }

    protected function getEndpoint()
    {
        $base = $this->getTestMode() ? $this->testEndpoint : $this->liveEndpoint;
        return $base;
    }

    protected function getHttpMethod()
    {
        return 'POST';
    }

    public function sendData($data)
    {
        $headers = [
            'Content-Type' => 'application/json; charset=utf-8',
            'Accept' => 'application/json',
            'Authorization' => 'Basic ' . base64_encode($this->getUserId() . ':' . $this->getPassword())
        ];
        $httpResponse = $this->httpClient->request(
            $this->getHttpMethod(),
            $this->getEndpoint(),
            $headers,
            json_encode($data)
        );

        return $this->response = new Response(
            $this,
            json_decode($httpResponse->getBody(), true),
            $httpResponse->getStatusCode()
        );
    }
}
