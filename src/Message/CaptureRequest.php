<?php

namespace Omnipay\Saferpay\Message;

class CaptureRequest extends AbstractRequest
{
    public function getData()
    {
        $this->validate('userId', 'password', 'customerId', 'token');
        $data = parent::getData();
        $obj = array(
            'RequestHeader' =>
                array(
                'SpecVersion' => '1.10',
                'CustomerId' => $data['customerId'],
                'RequestId' => uniqid(),
                'RetryIndicator' => 0,
            ),
            'Token' => $data['token'],
        );

        return $obj;
    }

    protected function getEndpoint()
    {
        return parent::getEndpoint() . '/api/Payment/v1/PaymentPage/Assert';
    }
}
