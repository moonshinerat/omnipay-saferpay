<?php

namespace Omnipay\Saferpay\Message;

class PurchaseRequest extends AbstractRequest
{
    // public function getData()
    // {
    //     $data = parent::getData();

    //     $data['paymentType'] = 'DB';

    //     return $data;
    // }

    public function getData()
    {
        $this->validate(
            'userId',
            'password',
            'customerId',
            'terminalId',
            'amount',
            'currency',
            'transactionId',
            'description',
            'transactionId',
            'returnUrl',
            'cancelUrl'
        );
        $data = parent::getData();
        $obj = array(
            'RequestHeader' =>
                array(
                'SpecVersion' => '1.10',
                'CustomerId' => $data['customerId'], // must change
                'RequestId' => uniqid(),
                'RetryIndicator' => 0,
            ),
            'TerminalId' => $data['terminalId'],
            'Payment' =>
                array(
                'Amount' =>
                    array(
                    'Value' => $data['amount'],
                    'CurrencyCode' => $data['currency'],
                ),
                'OrderId' => $data['transactionId'],
                'Description' => $data['description'],
            ),
            'ReturnUrls' =>
                array(
                'Success' => $data['returnUrl'],
                'Fail' => $data['cancelUrl'],
            ),
        );
        return $obj;
    }

    public function getEndpoint()
    {
        return parent::getEndpoint() . '/api/Payment/v1/PaymentPage/Initialize';
    }
}
