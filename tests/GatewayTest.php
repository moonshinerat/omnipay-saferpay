<?php

namespace Omnipay\Saferpay;

use Omnipay\Tests\GatewayTestCase;

class GatewayTest extends GatewayTestCase
{
    protected $gateway;

    public function setUp()
    {
        parent::setUp();
        $this->gateway = new Gateway($this->getHttpClient(), $this->getHttpRequest());
        $this->assertTrue(true);
    }

    public function testPurchase()
    {
        $request = $this->gateway->purchase(array('amount' => '10.00', 'currency' => 'USD'));
        $this->assertInstanceOf('Omnipay\Saferpay\Message\PurchaseRequest', $request);
        $this->assertSame('10.00', $request->getAmount());
        $this->assertSame('USD', $request->getCurrency());
    }

    public function testCapture()
    {
        $request = $this->gateway->capture(array('amount' => '10.00', 'currency' => 'USD'));
        $this->assertInstanceOf('Omnipay\Saferpay\Message\CaptureRequest', $request);
        $this->assertSame('10.00', $request->getAmount());
        $this->assertSame('USD', $request->getCurrency());
    }

}
