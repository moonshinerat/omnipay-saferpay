<?php

namespace Omnipay\Saferpay\Message;

use Omnipay\Tests\TestCase;

class CaptureRequestTest extends TestCase
{


    public function setUp()
    {
        $this->request = new CaptureRequest($this->getHttpClient(), $this->getHttpRequest());
        $this->request->initialize([
            'userId' => 'abc',
            'password' => 'xyz',
            'customerId' => '12345',
            'token' => 'o4rhti5sis0qd5hg2qldrr9cn',
        ]);
    }

    public function testGetData()
    {
        $response = $this->request->getData();
        $this->assertSame('12345', $response['RequestHeader']['CustomerId']);
    }

    public function testSend()
    {
        $this->setMockHttpResponse('CaptureSuccess.txt');

        $response = $this->request->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertFalse($response->isRedirect());
    }
}