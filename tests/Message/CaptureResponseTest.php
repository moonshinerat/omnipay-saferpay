<?php

namespace Omnipay\Saferpay\Message;

use Omnipay\Tests\TestCase;

class CaptureResponseTest extends TestCase
{
    public function testCaptureSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('CaptureSuccess.txt');
        $response = new Response($this->getMockRequest(), json_decode($httpResponse->getBody(), true), $httpResponse->getStatusCode());
        $this->assertTrue($response->isSuccessful());
        $this->assertSame('AUTHORIZED', $response->getData()['Transaction']['Status']);
        $this->assertEquals($response->getCode(), 200);
    }
    public function testCaptureFailure()
    {
        $httpResponse = $this->getMockHttpResponse('CaptureFailure.txt');
        $response = new Response($this->getMockRequest(), json_decode($httpResponse->getBody(), true), $httpResponse->getStatusCode());
        $this->assertFalse($response->isSuccessful());
        $this->assertEquals($response->getCode(), 403);
    }
}