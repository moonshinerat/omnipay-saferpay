<?php

namespace Omnipay\Saferpay\Message;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Omnipay\Tests\TestCase;

class PurchaseRequestTest extends TestCase
{
    public function setUp()
    {
        $this->request = new PurchaseRequest($this->getHttpClient(), $this->getHttpRequest());
        $this->request->initialize([
            'userId' => 'abc',
            'password' => 'xyz',
            'transactionId' => '12345',
            'customerId' => '12345',
            'terminalId' => '54321',
            'description' => "Order ID or invoice ID",
            'returnUrl' => 'http://localhost/payments/123',
            'cancelUrl' => 'http://localhost/payments/error',
            'amount' => '100',
            'currency' => 'USD'
        ]);
    }

    public function testGetData()
    {
        $response = $this->request->getData();
        $this->assertSame('54321', $response['TerminalId']);
    }

    public function testSend()
    {
        $this->setMockHttpResponse('PurchaseSuccess.txt');

        $response = $this->request->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->isRedirect());
    }
}