<?php

namespace Omnipay\Saferpay\Message;

use Omnipay\Tests\TestCase;

class PurchaseResponseTest extends TestCase
{
    public function testPurchaseSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('PurchaseSuccess.txt');
        $response = new Response($this->getMockRequest(), json_decode($httpResponse->getBody(), true), $httpResponse->getStatusCode());
        $this->assertTrue($response->isSuccessful());
        $this->assertTrue($response->isRedirect());
        $this->assertSame('cybfzv4wff4mo3e2ikdf3g5r8', $response->getToken());
        $this->assertEquals($response->getCode(), 200);
    }
    public function testPurchaseFailure()
    {
        $httpResponse = $this->getMockHttpResponse('PurchaseFailure.txt');
        $response = new Response($this->getMockRequest(), json_decode($httpResponse->getBody(), true), $httpResponse->getStatusCode());
        $this->assertFalse($response->isSuccessful());
        $this->assertFalse($response->isRedirect());
        $this->assertEquals($response->getCode(), 403);
    }
}